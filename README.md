# wave2rgb #
v1.0 R code version wavelength to RGB conversion

Code is imported as a function in R using the `source()` function. This is called as `wave2rgb()`.

The function takes a wavelength in nanometers (can be decimal), and returns `#RRGGBB` color.

This was modified from [Python code](http://codingmess.blogspot.com/2009/05/conversion-of-wavelength-in-nanometers.html), which was previously adapted from [Fortran](http://www.physics.sfasu.edu/astro/color/spectra.html).

## example usage ##

`wave2rgb(482)`

### To color text based on peak wavelength of a unimodal blue spectrum: ###

```
source("~/git/wave2rgb/wave2rgb.R")

isidella_biolum = read.table("~/git/biolum-spectra/cnidaria/isidella_sp_biolum_BU2020.txt", header=FALSE, sep="\t", skip=1)
sp_isidella_biolum = smooth.spline(isidella_biolum[,1], isidella_biolum[,2], df=36)
isidella_biolum_max = max(sp_isidella_biolum$y)

# get lambda max, and RGB color
isidella_lmax = sp_isidella_biolum$x[which(sp_isidella_biolum$y==isidella_biolum_max)]
isidella_lmax_color = wave2rgb(isidella_lmax)

# plot black line, add wavelength above top of curve
plot(sp_isidella_biolum$x, sp_isidella_biolum$y/isidella_biolum_max, type='l', xlim=c(350,670), ylim=c(0,1), xlab="", ylab="", cex.lab=1.3, cex.axis=1.3, frame.plot=FALSE)
text(isidella_lmax, 1, paste0(isidella_lmax, "nm"), pos=4, col=isidella_lmax_color, cex=1)

```

```
> isidella_lmax
[1] 482.78
> isidella_lmax_color
[1] "#00D6FF"
```

### To display a spectrum as a rainbow: ###

```
source("~/git/wave2rgb/wave2rgb.R")

isidella_biolum = read.table("~/git/biolum-spectra/cnidaria/isidella_sp_biolum_BU2020.txt", header=FALSE, sep="\t", skip=1)
sp_isidella_biolum = smooth.spline(isidella_biolum[,1], isidella_biolum[,2], df=36)
isidella_biolum_max = max(sp_isidella_biolum$y)

# get RGB for all wavelengths
isidella_colors = sapply( sp_isidella_biolum$x, wave2rgb)

# display as "histogram" mode, with each line having the color of the wavelength
plot(sp_isidella_biolum$x, sp_isidella_biolum$y/isidella_biolum_max, type='h', xlim=c(350,670), ylim=c(0,1), xlab="", ylab="", col=isidella_colors, cex.lab=1.3, cex.axis=1.3, frame.plot=FALSE )
```

![isidella_spectra_w_rainbow_v1.png](https://bitbucket.org/wrf/wave2rgb/raw/f5e4d6e9d70af74ae7941ce8756909e8a2e0ee38/images/isidella_spectra_w_rainbow_v1.png)

